﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Forces : MonoBehaviour
{
    public ManualVector3 wind = new ManualVector3(1.0f, 0, 0);
    public ManualVector3 gravity = new ManualVector3(0, -9.40f, 0);
    public ManualVector3 push = new ManualVector3(0.50f, 0, 0);
    public ManualVector3 pull = new ManualVector3(-0.20f, 0, 0);
    public ManualVector3 normalForce = new ManualVector3(0, 9.40f, 0);
    public ManualVector3 strongForce = new ManualVector3(10.0f, 9.40f, 0);

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}