﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    enum asteroidSizes
    {
        Small = 1,
        Medium = 2,
        Large = 3
    };

    public GameObject mediumAsteroidPrefab;
    public GameObject smallAsteroidPrefab;

    public Rigidbody2D rb2D;

    public float maximumPushForce;
    public float maximumSpinForce;

    public float screenTopBounds;
    public float screenBottomBounds;
    public float screenLeftBounds;
    public float screenRightBounds;

    public int asteroidSize;

    // Use this for initialization
    void Start()
    {
		Vector2 push = new Vector2(Random.Range(maximumPushForce * -1.0f, maximumPushForce), Random.Range(maximumPushForce * -1.0f, maximumPushForce));
        Vector2 spin = new Vector2(Random.Range(maximumSpinForce * -1.0f, maximumSpinForce), Random.Range(maximumSpinForce * -1.0f, maximumSpinForce));

        rb2D.AddForce(push);
        rb2D.AddForce(spin);

        asteroidSize = (int)asteroidSizes.Large;
    }

    // Update is called once per frame
    void Update()
    {
        CheckBounds();
	}

    private void CheckBounds()
    {
        Vector2 newPosition = transform.position;

        if (transform.position.y > screenTopBounds)
        {
            newPosition.y = screenBottomBounds;
        }

        if (transform.position.y < screenBottomBounds)
        {
            newPosition.y = screenTopBounds;
        }

        if (transform.position.x > screenRightBounds)
        {
            newPosition.x = screenLeftBounds;
        }

        if (transform.position.x < screenLeftBounds)
        {
            newPosition.x = screenRightBounds;
        }

        transform.position = newPosition;
    }

    void OnTriggerEnter2D(Collider2D collidedwith)
    {
        if(collidedwith.tag == "Bullet")
        {
            Destroy(collidedwith.gameObject);

            if (asteroidSize == (int)asteroidSizes.Large)
            {
                GameObject mediumAsteroid1 = Instantiate(mediumAsteroidPrefab, transform.position, transform.rotation);
                GameObject mediumAsteroid2 = Instantiate(mediumAsteroidPrefab, transform.position, transform.rotation);

                mediumAsteroid1.GetComponent<Asteroid>().asteroidSize = (int)asteroidSizes.Medium;
                mediumAsteroid2.GetComponent<Asteroid>().asteroidSize = (int)asteroidSizes.Medium;

                Destroy(gameObject);
            }

            else if (asteroidSize == (int)asteroidSizes.Medium)
            {
                GameObject smallAsteroid1 = Instantiate(mediumAsteroidPrefab, transform.position, transform.rotation);
                GameObject smallAsteroid2 = Instantiate(mediumAsteroidPrefab, transform.position, transform.rotation);

                smallAsteroid1.GetComponent<Asteroid>().asteroidSize = (int)asteroidSizes.Small;
                smallAsteroid2.GetComponent<Asteroid>().asteroidSize = (int)asteroidSizes.Small;

                Destroy(gameObject);
            }

            else if (asteroidSize == (int)asteroidSizes.Small)
            {

            }
        }
    }
}