﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    private float pushInput;
    private float turnInput;

    public Rigidbody2D rb2D;
    public float pushForce;
    public float rotationPushForce;

    public float screenTopBounds;
    public float screenBottomBounds;
    public float screenLeftBounds;
    public float screenRightBounds;

    public GameObject bullet;
    public float bulletForce;

    public float destructionForce;

    // Use this for initialization
    void Start()
    {
		
	}
	
	// Update is called once per frame
	void Update()
    {
        pushInput = Input.GetAxis("Vertical");
        turnInput = Input.GetAxis("Horizontal");

        transform.Rotate(Vector3.forward * turnInput * Time.deltaTime * rotationPushForce);

        CheckBounds();
        Shoot();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb2D.AddRelativeForce(Vector2.up * pushInput);
        //rb2D.AddTorque(turnInput * -1.0f);
    }

    void OnCollisionEnter2D(Collision2D collidedWith)
    {
        if(collidedWith.collider.tag == "Asteroid")
        {
            print(collidedWith.relativeVelocity.magnitude);

            if (collidedWith.relativeVelocity.magnitude > destructionForce)
            {
                print("Die!");
            }
        }


    }

    private void CheckBounds()
    {
        Vector2 newPosition = transform.position;

        if(transform.position.y > screenTopBounds)
        {
            newPosition.y = screenBottomBounds;
        }

        if (transform.position.y < screenBottomBounds)
        {
            newPosition.y = screenTopBounds;
        }

        if (transform.position.x > screenRightBounds)
        {
            newPosition.x = screenLeftBounds;
        }

        if (transform.position.x < screenLeftBounds)
        {
            newPosition.x = screenRightBounds;
        }

        transform.position = newPosition;
    }

    private void Shoot()
    {
        if(Input.GetKeyDown(KeyCode.Space) == true)
        {
            GameObject newBullet = Instantiate(bullet, transform.position + new Vector3(0, 1.0f, 0), transform.rotation);

            newBullet.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * bulletForce);

            Destroy(newBullet, 2.5f);
        }
    }
}