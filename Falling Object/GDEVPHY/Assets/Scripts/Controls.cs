﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : MonoBehaviour
{
    public float speed = 1.0f;

    public ManualVector3 up = new ManualVector3(0, 1.0f, 0);
    public ManualVector3 down = new ManualVector3(0, -1.0f, 0);
    public ManualVector3 left = new ManualVector3(-1.0f, 0.0f, 0);
    public ManualVector3 right = new ManualVector3(1.0f, 0, 0);

    private Movable movable;
    public float x, y;

    // Use this for initialization
    void Start()
    {
        x = y = 0;
        movable = GetComponent<Movable>();
    }

    // Update is called once per frame
    void Update()
    {
        ManualVector3 target = KeyboardControls();
        /*
        Movement(target * speed * Time.deltaTime);
        FaceDirection(target);*/

        x = target.x;
        y = target.y;

        movable.ApplyForce(target);
        FaceDirection(target);
    }

    private ManualVector3 KeyboardControls()
    {
        ManualVector3 target = new ManualVector3(0, 0, 0);

        if (Input.anyKey)
        {
            if (Input.GetKey(KeyCode.W))
            {
                target += up; 
            }

            if (Input.GetKey(KeyCode.A))
            {
                target += left;
            }

            if (Input.GetKey(KeyCode.S))
            {
                target += down;
            }

            if (Input.GetKey(KeyCode.D))
            {
                target += right;
            }

            target.Normalize();
        }
            return target;
    }

    private void Movement(ManualVector3 movement)
    {
        transform.position = new Vector3(transform.position.x + movement.x, transform.position.y + movement.y, transform.position.z + movement.z);
    }

    private void FaceDirection(ManualVector3 movement)
    {
        transform.rotation = Quaternion.Euler(0, 0, movement.getAngle() - 90.0f);
    }
}