﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movable : MonoBehaviour
{
    //[HideInInspector]
    public ManualVector3 Scale = new ManualVector3(0.5f, 0.5f, 0.5f);

    //[HideInInspector]
    public ManualVector3 Position = new ManualVector3();

    //[HideInInspector]
    public ManualVector3 Velocity = new ManualVector3();

    //[HideInInspector]
    public ManualVector3 Acceleration = new ManualVector3();

    public float mass = 5.0f;
    public float G = 0.5f;
    public bool useGravity;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        this.UpdateMotion();

        if(useGravity ==  true)
        {
            this.ApplyGravity(9.80f);
        }

        //this.ClampPosition();

        if(this.transform.position.y <= -1.15)
        {
            this.Velocity = new ManualVector3(0, 0, 0);
        }
    }

    public void ApplyGravity(float scalar = 0.1f)
    {
        this.Acceleration += (new ManualVector3(0, -scalar * mass, 0) / mass);
    }

    public void ApplyForce(ManualVector3 force)
    {
        this.Acceleration += (force / mass);
    }

    protected void UpdateMotion()
    {
        MoveObject(this.Velocity * Time.deltaTime);
        this.Velocity += this.Acceleration * Time.deltaTime;
        this.Acceleration *= 0;
    }

    protected void MoveObject(ManualVector3 force)
    {
        transform.position = new Vector3(transform.position.x + force.x, transform.position.y + force.y, transform.position.z + force.z);
    }

    public void ClampPosition()
    {
        float x = transform.position.x;
        float y = transform.position.y;

        if(x >= 10.0f)
        {
            x = -9.9f;
        }

        if(x <= -10.0f)
        {
            x = 9.9f;
        }

        if (y >= 7.1f)
        {
            y = -7.0f;
        }

        if (y <= -7.1f)
        {
            y = 7.0f;
        }

        transform.position = new Vector3(x, y, 0);
    }

    public ManualVector3 CalculateAttraction(Movable otherBody)
    {
        Vector3 force = transform.position = new Vector3(transform.position.x - otherBody.transform.position.x, transform.position.y - otherBody.transform.position.y, transform.position.z - otherBody.transform.position.z);

        ManualVector3 manualForce = new ManualVector3(transform.position.x, transform.position.y, transform.position.z);

        var distance = manualForce.GetLength();

        distance = Utilities.Constrain(distance, 1, 5);

        manualForce.Normalize();

        var strength = (G * mass * otherBody.mass) / (distance * distance);
        force *= strength;

        return manualForce;
    }
}