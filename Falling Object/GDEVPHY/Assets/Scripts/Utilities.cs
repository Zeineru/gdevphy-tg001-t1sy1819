﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utilities : MonoBehaviour
{
    public static float Constrain(float value, float min, float max)
    {
        if (value <= min)
        {
            return min;
        }

        else if (value >= max)
        {
            return max;
        }

        else
        {
            return value;
        }
    }
}
