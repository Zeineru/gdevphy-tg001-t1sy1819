﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomNumberGenerator : MonoBehaviour
{
    public static int GenerateInt(int min = 0, int max = 1)
    {
        return Random.Range(min, max + 1);
    }

    public static double GenerateDouble(float min = 0.0f, float max = 1.0f)
    {
        return Random.Range((max - min), min);
    }

    public static float GenerateFloat(float min = 0.0f, float max = 1.0f)
    {
        return Random.Range((max - min), min);
    }

    public static byte GenerateByte(int min = 0, int max = 1)
    {
        var value = Random.Range(min, max + 1);

        return (byte)value;
    }  
}