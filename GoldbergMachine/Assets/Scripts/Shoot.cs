﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    private bool hasShoot = false;

    public GameObject bullet;
    public float bulletForce;
    public float xAllowance;
    public float yAllowance;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space) == true && hasShoot == false)
        {
            //hasShoot = true;

            GameObject newBullet = Instantiate(bullet, transform.position + new Vector3(xAllowance, yAllowance, 0), transform.rotation);

            newBullet.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * bulletForce);

            //Destroy(newBullet, 2.5f);
        }
    }
}