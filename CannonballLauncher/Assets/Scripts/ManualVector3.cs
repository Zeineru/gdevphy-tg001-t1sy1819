﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManualVector3
{
    public float x;
    public float y;
    public float z;

    public ManualVector3()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    public ManualVector3(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public static ManualVector3 operator +(ManualVector3 left, ManualVector3 right)
    {
        return new ManualVector3(left.x + right.x,
            left.y + right.y,
            left.z + right.z);
    }

    public static ManualVector3 operator -(ManualVector3 left, ManualVector3 right)
    {
        return new ManualVector3(left.x - right.x,
            left.y - right.y,
            left.z - right.z);
    }

    public static ManualVector3 operator *(ManualVector3 left, ManualVector3 right)
    {
        return new ManualVector3(left.x * right.x,
            left.y * right.y,
            left.z * right.z);
    }

    public static ManualVector3 operator /(ManualVector3 left, ManualVector3 right)
    {
        return new ManualVector3(left.x / right.x,
            left.y / right.y,
            left.z / right.z);
    }

    public static ManualVector3 operator *(ManualVector3 left, float scalar)
    {
        return new ManualVector3(left.x * scalar,
            left.y * scalar,
            left.z * scalar);
    }

    public static ManualVector3 operator /(ManualVector3 left, float scalar)
    {
        return new ManualVector3(left.x / scalar,
            left.y / scalar,
            left.z / scalar);
    }

    public float GetLength()
    {
        return (float)Math.Sqrt((x * x) + (y * y) + (z * z));
    }

    public ManualVector3 Normalize()
    {
        float length = GetLength();
        if (x != 0) x /= length;
        if (y != 0) y /= length;
        if (z != 0) z /= length;

        return new ManualVector3(x, y, z);
    }

    public override string ToString()
    {
        return "x: " + x + " y: " + y + " z: " + z;
    }

    public float GetAngle()
    {
        return Mathf.Atan2(y, x) * Mathf.Rad2Deg;
    }
    public ManualVector3 ConvertAngle(float angle)
    {
        float length = this.GetLength();

        this.x = Mathf.Cos(angle * Mathf.Deg2Rad);
        this.y = Mathf.Sin(angle * Mathf.Deg2Rad);

        return this * length;
    }
}