﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private Collider2D c2D;

	// Use this for initialization
	void Start()
    {
        c2D = GetComponent<Collider2D>();
	}
	
	// Update is called once per frame
	void Update()
    {
		
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        ManualVector3 normal = new ManualVector3(collision.contacts[0].normal.x, collision.contacts[0].normal.y, 0);

        gameObject.GetComponent<Movable>().ApplyForce(normal);
    }
}