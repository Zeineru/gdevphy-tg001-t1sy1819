﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolidSurface : MonoBehaviour
{
    private float x;
    private float y;
    private float width;
    private float depth;

    public Movable bullet;
    public float drag;

    // Use this for initialization
    void Start ()
    {
        width = gameObject.transform.localScale.x;
        depth = gameObject.transform.localScale.y;

        x = gameObject.transform.position.x;
        y = gameObject.transform.position.y;
	}

    // Update is called once per frame
    void Update()
    {
        if(Collision(bullet))
        {
            var dragForce = gameObject.GetComponent<SolidSurface>().CalculateDragForce(bullet);

            bullet.GetComponent<Movable>().ApplyForce(dragForce);
        }
    }

    public bool Collision(Movable movable)
    {
        var p = movable.Position;

        return (p.x > this.x - this.width) && (p.x < this.x + this.width) && (p.y < this.y);
    }

    public ManualVector3 CalculateDragForce(Movable movable)
    {
        var speed = movable.Velocity.GetLength();
        var dragMagnitude = this.drag * speed * speed;

        var dragForce = movable.Velocity;
        dragForce *= -1;

        dragForce.Normalize();
        dragForce *= dragMagnitude;

        return dragForce;
    }
}