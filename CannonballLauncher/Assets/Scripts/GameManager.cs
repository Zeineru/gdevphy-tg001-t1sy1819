﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(GameObject.FindGameObjectsWithTag("Obstacle").Length <= 0)
        {
            StartCoroutine(RestartScene());
        }
	}

    IEnumerator RestartScene(float waitTime = 2.0f)
    {
        yield return new WaitForSeconds(2.0f);

        LoadScene();
    }

    private void LoadScene(int sceneNumber = 0)
    {
        SceneManager.LoadScene(sceneNumber);
    }
}