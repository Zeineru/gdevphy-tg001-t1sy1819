﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    public GameObject bullet;
    public float shootForce;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        AdjustShootForce();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            //SpawnBullet();

            StartCoroutine(DelayShoot());
        }
    }

    private void SpawnBullet()
    {
        GameObject clone = Instantiate(bullet);

        clone.transform.position = gameObject.transform.position;

        Movable m = clone.GetComponent<Movable>();

        float mForce = m.Position.GetAngle();

        m.ApplyForce(new ManualVector3(1.0f, 0, 0).ConvertAngle(transform.eulerAngles.z) * shootForce);

        //clone.GetComponent<Rigidbody2D>().AddForce(this.ConvertShootAngle(transform.eulerAngles.z) * shootForce);

        Destroy(clone, 10.0f);
    }

    private void AdjustShootForce()
    {
        if(Input.GetKeyDown(KeyCode.Q))
        {
            shootForce = shootForce - 100.0f;

            if(shootForce <= 1000)
            {
                shootForce = 1000;
            }
        }

        else if (Input.GetKeyDown(KeyCode.W))
        {
            shootForce = shootForce + 100.0f;

            if(shootForce >= 2500)
            {
                shootForce = 2500;
            }
        }
    }
    private IEnumerator DelayShoot(float waitTime = 0.3f)
    {
        yield return new WaitForSeconds(waitTime);

        SpawnBullet();
    }

    private float GetShooterLength()
    {
        float x = gameObject.transform.position.x * gameObject.transform.position.x;
        float y = gameObject.transform.position.y * gameObject.transform.position.y;
        float z = gameObject.transform.position.z * gameObject.transform.position.z;

        return (float)Mathf.Sqrt((x * x) + (y * y) + (z * z));
    }

    private Vector2 ConvertShootAngle(float angle)
    {
        float length = this.GetShooterLength();
        Vector2 value = new Vector3(1.0f, 0, 0);

        float x = gameObject.transform.position.x;
        float y = gameObject.transform.position.y;

        x = Mathf.Cos(angle * Mathf.Deg2Rad);
        y = Mathf.Sin(angle * Mathf.Deg2Rad);

        gameObject.transform.position = new Vector2(x, y);

        return gameObject.transform.position * length;
    }
}