﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBehavior : MonoBehaviour
{
	// Use this for initialization
	void Start()
    {
		
	}
	
	// Update is called once per frame
	void Update()
    {
        Vector3 velocity = new Vector3(gameObject.GetComponent<Movable>().Velocity.x, gameObject.GetComponent<Movable>().Velocity.y, gameObject.GetComponent<Movable>().Velocity.z);

        if (gameObject.GetComponent<Movable>().Acceleration == new ManualVector3(0, 0, 0))
        {
            gameObject.GetComponent<Movable>().ApplyForce(new ManualVector3(1.0f, 0, 0));
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Ground")
        {
            //print("Collided!");

            gameObject.GetComponent<Movable>().Acceleration = new ManualVector3(0, 0, 0);
        }

        if (collision.gameObject.tag == "Obstacle")
        {
            print("Collided!");

            gameObject.GetComponent<Movable>().Acceleration = new ManualVector3(0, 0, 0);

            Destroy(collision.gameObject);       
        }
    }
}