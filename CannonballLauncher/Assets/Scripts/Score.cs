﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Text scoreText;

	// Use this for initialization
	void Start()
    {
		
	}
	
	// Update is called once per frame
	void Update()
    {
        int targetsLeft = GameObject.FindGameObjectsWithTag("Obstacle").Length;

        scoreText.text = "Targets Left: " + targetsLeft;
	}
}