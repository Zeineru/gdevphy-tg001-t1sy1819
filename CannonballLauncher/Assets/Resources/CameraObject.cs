﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraObject : MonoBehaviour
{
    public Camera referenceCamera;
    public GameObject game;

	// Use this for initialization
	void Start ()
    {
        referenceCamera.GetComponent<Camera>();
        game = game.GetComponent<GameObject>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        this.transform.position = game.transform.position;
	}
}